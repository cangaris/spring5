SET @PREVIOUS_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS;
SET FOREIGN_KEY_CHECKS = 0;


LOCK TABLES `address` WRITE;
TRUNCATE `address`;
ALTER TABLE `address` DISABLE KEYS;
REPLACE INTO `address` (`address_id`, `city`, `street`) VALUES 
	(1,'Warszawa','Marszałkowska 4/9'),
	(2,'Gdańsk','Toruńska 3/9'),
	(3,'Wrocław','Cwiartki 3/4'),
	(4,'Kraków','Krakowska 5/8'),
	(5,'Poznań','Poznańska 1/1'),
	(6,'Toruń','Wrocławska 8/9');
ALTER TABLE `address` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `phone_number` WRITE;
TRUNCATE `phone_number`;
ALTER TABLE `phone_number` DISABLE KEYS;
REPLACE INTO `phone_number` (`phone_id`, `number`, `prefix`, `user_id`) VALUES 
	(11,'543543543','543543',15),
	(13,'5555555','+55',14),
	(22,'99999993','+993',17),
	(23,'88888883','+883',17);
ALTER TABLE `phone_number` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `product_category` WRITE;
TRUNCATE `product_category`;
ALTER TABLE `product_category` DISABLE KEYS;
REPLACE INTO `product_category` (`id`, `description`, `img_uri`, `name`) VALUES 
	(2,'Najlepsze telefony na rynku :)','https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large','Phones'),
	(3,'Najlepsze laptopy na rynku :)','https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large','Laptops');
ALTER TABLE `product_category` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `product` WRITE;
TRUNCATE `product`;
ALTER TABLE `product` DISABLE KEYS;
REPLACE INTO `product` (`id`, `description`, `img_uri`, `name`, `price`, `category_id`) VALUES 
	(3,'Super iPhone, polecam :)','https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large','iPhone X',2000.00,2),
	(4,'Najlepszy iphone 11','https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large','iPhone 11',3000.00,2),
	(5,'Najlepszy iphone','https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large','iPhone 13',3000.00,2),
	(6,'Najlepszy iphone 15 na rynku, super okazja polecam ;)','https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large','iPhone 15',5000.00,2),
	(7,'Najlepszy macbook','https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large','Macbook Pro',6000.00,3);
ALTER TABLE `product` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user` WRITE;
TRUNCATE `user`;
ALTER TABLE `user` DISABLE KEYS;
REPLACE INTO `user` (`user_id`, `email`, `first_name`, `last_name`, `detail_id`, `password`) VALUES 
	(14,'dam@kowalski.pl','Damian','Kowalski',16,'secretPass'),
	(15,'dam@nowak.pl','Damian','Nowak',13,'secretPass'),
	(17,'damian@kowalski.pl','Damian','Kowalski',21,'secretPass');
ALTER TABLE `user` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user_address_pivot` WRITE;
TRUNCATE `user_address_pivot`;
ALTER TABLE `user_address_pivot` DISABLE KEYS;
REPLACE INTO `user_address_pivot` (`user_id`, `address_id`) VALUES 
	(15,4),
	(14,6),
	(17,4);
ALTER TABLE `user_address_pivot` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user_details` WRITE;
TRUNCATE `user_details`;
ALTER TABLE `user_details` DISABLE KEYS;
REPLACE INTO `user_details` (`user_details_id`, `personal_number`, `tax_number`) VALUES 
	(13,'545435','87687686'),
	(16,'888888','333333'),
	(21,'5435435433','6576576573');
ALTER TABLE `user_details` ENABLE KEYS;
UNLOCK TABLES;




SET FOREIGN_KEY_CHECKS = @PREVIOUS_FOREIGN_KEY_CHECKS;



package pl.cansoft.spring5.models;

public enum Role {
    ADMIN,
    MANAGER,
    CLIENT
}

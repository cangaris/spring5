package pl.cansoft.spring5.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class IdDto { // DTO - Data Transfer Object
    private Integer id;
}

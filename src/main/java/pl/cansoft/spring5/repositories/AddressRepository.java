package pl.cansoft.spring5.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cansoft.spring5.models.Address;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}

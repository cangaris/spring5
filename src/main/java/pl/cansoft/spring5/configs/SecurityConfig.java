package pl.cansoft.spring5.configs;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import pl.cansoft.spring5.services.UserService;

@Configuration // tylko po to aby ponieść klasę na start aplikacji jako bean
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
class SecurityConfig {

    private static final String[] PUBLIC_MVC_URI = {
        "/", "/categories", "/aboutUs", "/contact",
        "/productDetails/{id}", "/categoryDetails/{id}"
    };
    final UserService userService;

    @Bean
    public UserDetailsService userDetailsService() {
        return email -> userService.findByEmail(email)
            .map(user -> User.withUsername(user.getEmail())
                .password(user.getPassword()) // secretPass
                .roles(user.getRole().name())
                .build())
            .orElseThrow(() -> new UsernameNotFoundException(""));
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Order(2)
    @Bean
    public SecurityFilterChain filterChainMvc(HttpSecurity http) throws Exception {
        return http
            .authorizeHttpRequests(auth -> auth
                .requestMatchers(PUBLIC_MVC_URI).permitAll()
                .anyRequest().authenticated()
            )
            .formLogin(form -> form
                .loginPage("/login")
                .successForwardUrl("/")
                .defaultSuccessUrl("/")
                .permitAll()
            )
            .logout(form -> form
                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                .permitAll()
            )
            .cors().and()
            .csrf().and()
            .rememberMe().rememberMeParameter("rememberMe").and()
            .build();
    }

    @Order(1)
    @Bean
    public SecurityFilterChain filterChainRest(HttpSecurity http) throws Exception {
        return http
            .securityMatcher("/api/**")
            .authorizeHttpRequests(auth -> auth.anyRequest().authenticated())
            .formLogin(form -> form
                .loginPage("/api/login")
                .successHandler((request, response, authentication) ->
                    response.setStatus(HttpStatus.NO_CONTENT.value())) // 204
                .failureHandler((request, response, exception) ->
                    response.setStatus(HttpStatus.BAD_REQUEST.value())) // 400
                .permitAll()
            )
            .logout(form -> form
                .logoutUrl("/api/logout")
                .logoutSuccessHandler((request, response, authentication) ->
                    response.setStatus(HttpStatus.NO_CONTENT.value())) // 204
                .permitAll()
            )
            .exceptionHandling()
            .authenticationEntryPoint((request, response, authException) ->
                response.setStatus(HttpStatus.UNAUTHORIZED.value())) // 401
            .and()
            .cors().and()
            .csrf(httpSecurityCsrfConfigurer ->
                httpSecurityCsrfConfigurer.ignoringRequestMatchers("/api/**")
            )
            .rememberMe().rememberMeParameter("rememberMe").and()
            .build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        var configuration = new CorsConfiguration();
        configuration.setAllowedHeaders(List.of("*"));
        configuration.setAllowedOrigins(List.of("http://localhost:4200"));
        configuration.setAllowedMethods(List.of("DELETE", "GET", "POST", "PUT", "OPTION"));
        configuration.setAllowCredentials(true);
        var source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}

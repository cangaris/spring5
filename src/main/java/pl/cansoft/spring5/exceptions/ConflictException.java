package pl.cansoft.spring5.exceptions;

public class ConflictException extends RuntimeException {
    public ConflictException() {
        super("Dany rekord już istnieje w bazie");
    }

    public ConflictException(String message) {
        super(message);
    }
}

package pl.cansoft.spring5.exceptions;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("Element nie został odnaleziony");
    }
}

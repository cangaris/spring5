package pl.cansoft.spring5.mappers;

import java.util.ArrayList;
import java.util.List;
import pl.cansoft.spring5.models.Address;
import pl.cansoft.spring5.models.PhoneNumber;
import pl.cansoft.spring5.models.User;
import pl.cansoft.spring5.models.UserDetails;
import pl.cansoft.spring5.models.dto.UserSaveDto;

public class UserSaveDtoUserMapper {

    public static User fromDtoToEntity(UserSaveDto userSaveDto) {
        var details = UserDetails.builder()
            .taxNumber(userSaveDto.getTaxNumber())
            .personalNumber(userSaveDto.getPersonalNumber())
            .build();
        var phones = new ArrayList<PhoneNumber>();
        for (int i = 0; i < userSaveDto.getPrefix().size(); i++) {
            var phoneNumber = PhoneNumber.builder()
                .prefix(userSaveDto.getPrefix().get(i))
                .number(userSaveDto.getNumber().get(i))
                .build();
            phones.add(phoneNumber);
        }
        var addresses = List.of(
            Address.builder()
                .addressId(userSaveDto.getAddress())
                .build()
        );
        return User.builder()
            .firstName(userSaveDto.getFirstName())
            .lastName(userSaveDto.getLastName())
            .email(userSaveDto.getEmail())
            .password("secretPass")
            .details(details)
            .phoneNumber(phones)
            .address(addresses)
            .build();
    }
}

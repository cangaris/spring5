package pl.cansoft.spring5.mappers;

import pl.cansoft.spring5.models.Product;
import pl.cansoft.spring5.models.ProductCategory;
import pl.cansoft.spring5.models.dto.ProductDto;

public class ProductToProductDtoMapper {

    public static ProductDto convertTo(Product product) {
        return ProductDto.builder()
            .id(product.getId())
            .name(product.getName())
            .desc(product.getDesc())
            .price(product.getPrice())
            .imgUri(product.getImgUri())
            .categoryId(product.getCategory() != null ? product.getCategory().getId() : null)
//            .categoryId(Optional.ofNullable(product.getCategory())
//                .map(ProductCategory::getId)
//                .orElse(null))
            .build();
    }

    public static Product convertTo(ProductDto productDto) {
        return Product.builder()
            .id(productDto.getId())
            .name(productDto.getName())
            .desc(productDto.getDesc())
            .price(productDto.getPrice())
            .imgUri(productDto.getImgUri())
            .category(ProductCategory.builder()
                .id(productDto.getCategoryId())
                .build())
            .build();
    }
}

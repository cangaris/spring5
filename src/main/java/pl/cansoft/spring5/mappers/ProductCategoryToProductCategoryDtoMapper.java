package pl.cansoft.spring5.mappers;

import pl.cansoft.spring5.models.ProductCategory;
import pl.cansoft.spring5.models.dto.ProductCategoryDto;

public class ProductCategoryToProductCategoryDtoMapper {

    public static ProductCategoryDto convertTo(ProductCategory productCategory) {
        return ProductCategoryDto.builder()
            .id(productCategory.getId())
            .name(productCategory.getName())
            .desc(productCategory.getDesc())
            .imgUri(productCategory.getImgUri())
            .build();
    }

    public static ProductCategory convertTo(ProductCategoryDto productCategory) {
        return ProductCategory.builder()
            .id(productCategory.getId())
            .name(productCategory.getName())
            .desc(productCategory.getDesc())
            .imgUri(productCategory.getImgUri())
            .build();
    }
}

package pl.cansoft.spring5.services;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring5.models.ProductCategory;

public interface ProductCategoryService {
    List<ProductCategory> findAllCategoryProducts();

    boolean existsProductCategoryByName(ProductCategory productCategory);

    ProductCategory insertProductCategory(ProductCategory productCategory);

    void updateProductCategory(Integer categoryId, ProductCategory categoryForm);

    void removeProductCategory(Integer categoryId);

    Optional<ProductCategory> findProductCategoryById(Integer categoryId);
}

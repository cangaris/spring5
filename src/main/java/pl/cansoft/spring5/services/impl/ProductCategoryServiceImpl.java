package pl.cansoft.spring5.services.impl;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.cansoft.spring5.exceptions.ConflictException;
import pl.cansoft.spring5.models.ProductCategory;
import pl.cansoft.spring5.repositories.ProductCategoryRepository;
import pl.cansoft.spring5.services.ProductCategoryService;

// IoC
@RequiredArgsConstructor
@Service // Bean (java ee) / Component (spring)
class ProductCategoryServiceImpl implements ProductCategoryService {

    final ProductCategoryRepository productCategoryRepository;

    @Override
    public List<ProductCategory> findAllCategoryProducts() {
        return productCategoryRepository.findAll();
    }

    @Override
    public boolean existsProductCategoryByName(ProductCategory productCategory) {
        return productCategoryRepository.existsByName(productCategory.getName());
    }

    @Override
    public ProductCategory insertProductCategory(ProductCategory productCategory) {
        var categoryExists = existsProductCategoryByName(productCategory);
        if (!categoryExists) {
            productCategory.setId(null);
            return productCategoryRepository.save(productCategory); // insert!
        } else {
            throw new ConflictException();
            // fixme: implementacja MVC zostanie zdestabilizowana powyższym błędem
            // todo: komunikat dla usera?
            // return null; // fixme: zwaracy byl null dla MVC
        }
    }

    @Override
    public void updateProductCategory(Integer categoryId, ProductCategory categoryForm) {
        categoryForm.setId(categoryId);
        productCategoryRepository.save(categoryForm); // update!
    }

    @Override
    public void removeProductCategory(Integer categoryId) {
        productCategoryRepository.deleteById(categoryId);
    }

    @Override
    public Optional<ProductCategory> findProductCategoryById(Integer categoryId) {
        return productCategoryRepository.findById(categoryId);
    }
}

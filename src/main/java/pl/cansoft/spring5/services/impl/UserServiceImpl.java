package pl.cansoft.spring5.services.impl;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cansoft.spring5.models.Address;
import pl.cansoft.spring5.models.User;
import pl.cansoft.spring5.repositories.AddressRepository;
import pl.cansoft.spring5.repositories.UserRepository;
import pl.cansoft.spring5.services.UserService;

/**
 * class UserService -> class UserServiceImpl
 * interface IUserService -> interface UserService
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
class UserServiceImpl implements UserService {

    final UserRepository userRepository; // <- UserRepositoryImpl
    final AddressRepository addressRepository;
    @Lazy
    final PasswordEncoder passwordEncoder; // <- BCryptPasswordEncoder

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<Address> getAddresses() {
        return addressRepository.findAll();
    }

    @Override
    public Optional<User> getUser(Integer userId) {
        return userRepository.findById(userId);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void saveAddress(Address address) {
        addressRepository.save(address);
    }

    @Override
    public void saveUser(User user) {
        user.setUserId(null);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void updateUser(Integer userId, User user) {
        user.setUserId(userId);
        userRepository.save(user);
    }

    @Override
    public void removeUserById(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }
}

package pl.cansoft.spring5.services.impl;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.cansoft.spring5.exceptions.ConflictException;
import pl.cansoft.spring5.models.Product;
import pl.cansoft.spring5.repositories.ProductRepository;
import pl.cansoft.spring5.services.ProductCategoryService;
import pl.cansoft.spring5.services.ProductService;

@RequiredArgsConstructor
@Service // IoC
class ProductServiceImpl implements ProductService {

    final ProductRepository productRepository;
    final ProductCategoryService productCategoryService;

    @Override
    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public boolean existsProductByName(Product productFrom) {
        return productRepository.existsByName(productFrom.getName());
    }

    @Override
    public Product insertProduct(Product productFrom) {
        var productExists = existsProductByName(productFrom);
        if (!productExists) {
            productFrom.setId(null);
            return productRepository.save(productFrom); // sql: insert jeżeli obiekt NIE posiada id
        } else {
            throw new ConflictException();
            // fixme: implementacja MVC zostanie zdestabilizowana powyższym błędem
            // todo: komunikat dla usera?
            // return null; // fixme: zwaracy byl null dla MVC
        }
    }

    @Override
    public void updateProduct(Product productFrom, Integer productId) {
        productFrom.setId(productId);
        productRepository.save(productFrom); // sql: update jeżeli obiekt posiada id
    }

    @Override
    public void removeProduct(Integer productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public Optional<Product> findProductById(Integer productId) {
        return productRepository.findById(productId);
    }

    @Override
    public List<Product> findProductByCategoryId(Integer categoryId) {
        return productRepository.findAllByCategoryId(categoryId);
    }
}

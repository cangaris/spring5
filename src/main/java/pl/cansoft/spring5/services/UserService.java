package pl.cansoft.spring5.services;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring5.models.Address;
import pl.cansoft.spring5.models.User;

public interface UserService {
    List<User> getUsers();

    List<Address> getAddresses();

    Optional<User> getUser(Integer userId);

    Optional<User> findByEmail(String email);

    void saveAddress(Address address);

    void saveUser(User user);

    void updateUser(Integer userId, User user);

    void removeUserById(Integer userId);

    boolean existsByEmail(String email);
}

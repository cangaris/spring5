package pl.cansoft.spring5.services;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring5.models.Product;

public interface ProductService {
    List<Product> findAllProducts();

    boolean existsProductByName(Product productFrom);

    Product insertProduct(Product productFrom);

    void updateProduct(Product productFrom, Integer productId);

    void removeProduct(Integer productId);

    Optional<Product> findProductById(Integer productId);

    List<Product> findProductByCategoryId(Integer categoryId);
}

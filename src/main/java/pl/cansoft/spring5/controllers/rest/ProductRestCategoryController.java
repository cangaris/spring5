package pl.cansoft.spring5.controllers.rest;

import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring5.exceptions.NotFoundException;
import pl.cansoft.spring5.mappers.ProductCategoryToProductCategoryDtoMapper;
import pl.cansoft.spring5.models.dto.IdDto;
import pl.cansoft.spring5.models.dto.ProductCategoryDto;
import pl.cansoft.spring5.services.ProductCategoryService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/category")
class ProductRestCategoryController {

    final ProductCategoryService productCategoryService;

    @GetMapping
    public ResponseEntity<List<ProductCategoryDto>> getCategories() {
        var categories = productCategoryService.findAllCategoryProducts().stream()
            .map(ProductCategoryToProductCategoryDtoMapper::convertTo)
            .toList();
        return ResponseEntity.ok(categories);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductCategoryDto> getCategory(@PathVariable Integer id) {
        return productCategoryService.findProductCategoryById(id)
            .map(ProductCategoryToProductCategoryDtoMapper::convertTo) // optional present
            .map(ResponseEntity::ok)
            .orElseThrow(NotFoundException::new); // if optional empty then throw error
    }

    @PostMapping
    public ResponseEntity<IdDto> addCategory(@Valid @RequestBody ProductCategoryDto productCategoryDto) {
        var productCategory = ProductCategoryToProductCategoryDtoMapper.convertTo(productCategoryDto);
        productCategory = productCategoryService.insertProductCategory(productCategory);
        return ResponseEntity.status(HttpStatus.CREATED) // 201
            .body(IdDto.builder()
                .id(productCategory.getId())
                .build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> editCategory(@PathVariable Integer id,
                                             @Valid @RequestBody ProductCategoryDto productCategoryDto) {
        var productCategory = ProductCategoryToProductCategoryDtoMapper.convertTo(productCategoryDto);
        productCategoryService.updateProductCategory(id, productCategory);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeCategory(@PathVariable Integer id) {
        productCategoryService.removeProductCategory(id);
        return ResponseEntity.noContent().build();
    }
}

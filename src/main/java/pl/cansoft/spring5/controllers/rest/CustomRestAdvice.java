package pl.cansoft.spring5.controllers.rest;

import java.util.List;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.cansoft.spring5.exceptions.ConflictException;
import pl.cansoft.spring5.exceptions.NotFoundException;
import pl.cansoft.spring5.models.dto.ErrorDto;

@RestControllerAdvice
class CustomRestAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDto> handleException(MethodArgumentNotValidException exception) {
        var errors = exception.getAllErrors().stream()
            .map(DefaultMessageSourceResolvable::getDefaultMessage)
            .toList();
        var errorDto = ErrorDto.builder()
            .messages(errors)
            .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<ErrorDto> handleException(ConflictException exception) {
        var errorDto = ErrorDto.builder()
            .messages(List.of(exception.getMessage()))
            .build();
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errorDto);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDto> handleException(NotFoundException exception) {
        var errorDto = ErrorDto.builder()
            .messages(List.of(exception.getMessage()))
            .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorDto);
    }
}

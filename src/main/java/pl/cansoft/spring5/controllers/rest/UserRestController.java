package pl.cansoft.spring5.controllers.rest;

import java.security.Principal;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring5.exceptions.NotFoundException;
import pl.cansoft.spring5.models.User;
import pl.cansoft.spring5.services.UserService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/user")
class UserRestController {

    final UserService userService;

    @GetMapping("/who-am-i")
    public ResponseEntity<User> whoAmI(Principal principal) {
        return userService.findByEmail(principal.getName())
            .map(ResponseEntity::ok)
            .orElseThrow(NotFoundException::new);
    }
}

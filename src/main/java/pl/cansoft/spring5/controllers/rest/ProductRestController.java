package pl.cansoft.spring5.controllers.rest;

import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring5.exceptions.NotFoundException;
import pl.cansoft.spring5.mappers.ProductToProductDtoMapper;
import pl.cansoft.spring5.models.dto.IdDto;
import pl.cansoft.spring5.models.dto.ProductDto;
import pl.cansoft.spring5.services.ProductService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/product")
class ProductRestController {

    final ProductService productService; // <- ProductServiceImpl

    @GetMapping
    public ResponseEntity<List<ProductDto>> getProducts() {
        var productDtoList = productService.findAllProducts().stream()
            .map(ProductToProductDtoMapper::convertTo)
            .toList();

        return ResponseEntity.ok(productDtoList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> getProduct(@PathVariable Integer id) {
        var optionalProduct = productService.findProductById(id);
        if (optionalProduct.isPresent()) {
            var product = optionalProduct.get();
            // return ResponseEntity.status(HttpStatus.OK).body(product);
            var productDto = ProductToProductDtoMapper.convertTo(product);
            return ResponseEntity.ok(productDto);
        } else {
            // return ResponseEntity.status(HttpStatus.NOT_FOUND).build(); // 404
            // return ResponseEntity.status(404).build(); // 404
            // return ResponseEntity.notFound().build(); // 404
            throw new NotFoundException();
        }
    }

    @PostMapping
    public ResponseEntity<IdDto> addProduct(@Valid @RequestBody ProductDto productDto) {
        var product = ProductToProductDtoMapper.convertTo(productDto);
        product = productService.insertProduct(product);
        // ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        return ResponseEntity.ok(IdDto.builder()
            .id(product.getId())
            .build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> editProduct(@PathVariable Integer id, @Valid @RequestBody ProductDto productDto) {
        var product = ProductToProductDtoMapper.convertTo(productDto);
        productService.updateProduct(product, id);
        return ResponseEntity.noContent().build(); // 204
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeProduct(@PathVariable Integer id) {
        productService.removeProduct(id);
        return ResponseEntity.noContent().build();
    }
}

package pl.cansoft.spring5.controllers;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.cansoft.spring5.mappers.UserSaveDtoUserMapper;
import pl.cansoft.spring5.models.Address;
import pl.cansoft.spring5.models.dto.UserSaveDto;
import pl.cansoft.spring5.services.UserService;

@Controller
@RequiredArgsConstructor
class UserController {

    final UserService userService; // <- UserServiceImpl

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @GetMapping("/user/all")
    public String getUsers(Model model) {
        var users = userService.getUsers();
        model.addAttribute("users", users);
        return "user/users";
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @GetMapping("/user/id/{id}")
    public String getUsers(@PathVariable Integer id, Model model) {
        var userOptional = userService.getUser(id);
        if (userOptional.isPresent()) {
            model.addAttribute("user", userOptional.get());
        } else {
            // todo: 404
        }
        return "user/user";
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/user/add")
    public String addUserForm(Model model) {
        model.addAttribute("addresses", userService.getAddresses());
        return "user/save-user";
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/user/edit/{userId}")
    public String editUserForm(@PathVariable Integer userId, Model model) {
        var optional = userService.getUser(userId);
        if (optional.isPresent()) {
            model.addAttribute("user", optional.get());
        } else {
            // todo: 404
        }
        model.addAttribute("addresses", userService.getAddresses());
        return "user/save-user";
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/user/address/add")
    public String saveUserAddress(@Valid Address address) {
        userService.saveAddress(address);
        return "redirect:/user/add";
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/user/save")
    public String saveUser(UserSaveDto userSaveDto, RedirectAttributes redirectAttributes) {
        var user = UserSaveDtoUserMapper.fromDtoToEntity(userSaveDto);
        if (userService.existsByEmail(userSaveDto.getEmail())) {
            redirectAttributes.addFlashAttribute("errorMessage", "Email jest zajęty");
            redirectAttributes.addFlashAttribute("user", user);
            return "redirect:/user/add";
        } else {
            userService.saveUser(user);
            return "redirect:/user/all";
        }
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/user/update/{userId}")
    public String userUpdate(@PathVariable Integer userId, UserSaveDto userSaveDto) {
        var user = UserSaveDtoUserMapper.fromDtoToEntity(userSaveDto);
        userService.updateUser(userId, user);
        return "redirect:/user/all";
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/user/remove/{userId}")
    public String removeUserById(@PathVariable Integer userId) {
        userService.removeUserById(userId);
        return "redirect:/user/all";
    }
}

package pl.cansoft.spring5.services.impl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.cansoft.spring5.exceptions.ConflictException;
import pl.cansoft.spring5.models.Product;
import pl.cansoft.spring5.repositories.ProductRepository;
import pl.cansoft.spring5.services.ProductCategoryService;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @InjectMocks
    ProductServiceImpl productService;
    @Mock
    ProductRepository productRepository;
    @Mock
    ProductCategoryService productCategoryService;

    @DisplayName("should call findAll from repository")
    @Test
    void findAllProducts() {
        // given
        // when
        productService.findAllProducts();
        // then
        Mockito.verify(productRepository).findAll();
    }

    @Test
    void existsProductByName() {
        // given
        var product = Product.builder()
            .name("test")
            .build();
        // when
        productService.existsProductByName(product);
        // then
        Mockito.verify(productRepository).existsByName("test");
    }

    @Test
    void insertProduct() {
        // given
        var product = Product.builder()
            .name("test")
            .build();
        var savedProduct = Product.builder()
            .id(89)
            .name("test")
            .build();
        // when
        Mockito.when(productRepository.existsByName("test")).thenReturn(false);
        Mockito.when(productRepository.save(product)).thenReturn(savedProduct);
        var response = productService.insertProduct(product);
        // then
        Mockito.verify(productRepository).save(product);
        Assertions.assertThat(response.getName()).isEqualTo("test");
        Assertions.assertThat(response.getId()).isNotNull();
    }

    @Test
    void insertProduct_exception() {
        // given
        var product = Product.builder()
            .name("test")
            .build();
        // when
        Mockito.when(productRepository.existsByName("test")).thenReturn(true);
        // then
        Assertions
            .assertThatThrownBy(() -> productService.insertProduct(product))
            .isInstanceOf(ConflictException.class);
    }

    @Test
    void updateProduct() {
        // given
        var productId = 3;
        var product = Product.builder()
            .id(productId)
            .build();
        // when
        productService.updateProduct(product, productId);
        // then
        Mockito.verify(productRepository).save(product);
    }

    @Test
    void removeProduct() {
        // given
        var productId = 5;
        // when
        productService.removeProduct(productId);
        // then
        Mockito.verify(productRepository).deleteById(5);
    }

    @Test
    void findProductById() {
        // given
        var productId = 67;
        // when
        productService.findProductById(productId);
        // then
        Mockito.verify(productRepository).findById(67);
    }

    @Test
    void findProductByCategoryId() {
        // given
        var productId = 167;
        // when
        productService.findProductByCategoryId(productId);
        // then
        Mockito.verify(productRepository).findAllByCategoryId(167);
    }
}

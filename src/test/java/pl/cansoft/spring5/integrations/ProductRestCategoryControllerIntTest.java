package pl.cansoft.spring5.integrations;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ProductRestCategoryControllerIntTest {

    private final static String JSON = """
        [
            {"id": 1, "name":"Drony","desc":"Najlepsze drony na rynku :)","imgUri":"https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large"}
        ]
        """;
    private final static String POST_ADD_JSON = """
        {"name":"Drony","desc":"Najlepsze drony na rynku :)","imgUri":"https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large"}
        """;

    private final static String POST_ADDED_JSON = """
        {"id": 1}
        """;

    private final static String PUT_UPDATE_JSON = """
        {"id": 1, "name":"Karty graficzne","desc":"Najlepsze karty na rynku :)","imgUri":"https://guide-images.cdn.ifixit.com/igi/o4OjCNmNeOhvsS1P.large"}
        """;

    @Autowired
    MockMvc mockMvc;

    @Order(1)
    @WithMockUser(roles = "ADMIN")
    @Test
    void saveCategory() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/category")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(POST_ADD_JSON)
            )
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andExpect(MockMvcResultMatchers.content().json(POST_ADDED_JSON));
    }

    @Order(2)
    @WithMockUser(roles = "ADMIN")
    @Test
    void getAllCategories() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/category"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(JSON));
    }

    @Order(3)
    @WithMockUser(roles = "ADMIN")
    @Test
    void updateCategory() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/api/category/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(PUT_UPDATE_JSON)
            )
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @WithMockUser(roles = "ADMIN")
    @Order(4)
    @Test
    void getUpdatedCategoryById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/category/1"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(PUT_UPDATE_JSON));
    }


}

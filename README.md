<p>Zadanie 1:</p>
<ol>
<li>Utworzyć klasę ProductCategory (id, name, desc, imgUri)</li>
<li>Utworzyć kontroler ProductCategoryController</li>
<li>Utworzyć endpoint listujący kategorie</li>
<li>Utworzyć template (HTML) dla listingu kategorii</li>
<li>Utworzyć endpoint dodający kategorie</li>
<li>Utworzyć template (fragment HTML) z formularzem dodawnia kategorii</li>
<li>Utworzyć endpoint usuwający kategorie</li>
<li>Dodać enpoint z kategoriami do menu głównego</li>
<li>Endpoint z kategoriami również posiada na górze linki z menu</li>
</ol>
<hr />
<p>Zadanie 2 (domowe): Na podstawie: /productDetails</p>
<ol>
<li>Do strony /categories dla listy dodać link kierujący do szczegółów kategorii</li>
<li>Utworzyć endpoint wraz z templatem wizualizującym szczegóły kategorii</li>
<li>Na widoku szczegółów kategorii widzimy: obrazek, tytuł, opis</li>
<li>Pamiętać o dodaniu menu głównego do szczegółów kategorii</li>
</ol>
<hr />
<p>https://www.jetbrains.com/help/idea/saving-and-reverting-changes.html#actions-on-save</p>
<p>https://www.samouczekprogramisty.pl/solid-czyli-dobre-praktyki-w-programowaniu-obiektowym/</p>
<p>https://www.baeldung.com/java-optional</p>
<hr />
<p>Zadanie 3: Na podstawie: /editProduct i /editedProduct</p>
<ol>
<li>Na widoku kategorii obok przyciksu usuwania dodajemy link do formularza edycji</li>
<li>Utworzyć endpoint [GET] /editCategory serwujący formularz edycji kategorii</li>
<li>Utworzyć endpoint [POST] /editedCategory łapiący dane z formularza i edytujący dane na liście</li>
<li>Pamiętać o zasileniu formularza edycji danymi z wcześniej istniejącego obiektu (th:value)</li>
<li>Pamiętać o zmiennym action dla formularza (raz będzie wysyłał dane do edycji a raz do dodania) (th:action)</li>
</ol>
<hr />
<p>Zadanie 4: ProductCategoryController i ProductCategoryService</p>
<ol>
<li>Utworzyć ProductService</li>
<li>Przenieść funckje operujące na danych z ProductController do ProductService</li>
<li>Adnotować serwis jako "bean" (IoC)</li>
<li>Poprosić o zależność do ProductService w klasie ProductController (DI)</li>
</ol>
<hr />
<p>https://www.baeldung.com/spring-dependency-injection</p>
<p>https://nofluffjobs.com/pl/log/wiedza-it/dependency-injection-w-springu/</p>
<p>https://bykowski.pl/wp-content/uploads/2021/05/spring-context-hierarchy.jpg</p>
<p>https://www.baeldung.com/spring-boot-h2-database</p>
<p>http://localhost:8085/h2-console</p>
<p>https://www.baeldung.com/sql-logging-spring-boot</p>
<p>https://www.baeldung.com/hibernate-identifiers</p>
<p>https://docs.spring.io/spring-data/jpa/docs/current/reference/html/</p>
<p>https://docs.spring.io/spring-boot/docs/1.1.0.M1/reference/html/howto-database-initialization.html</p>
<hr />
<p>Zadanie 5: Na podstawie ProductRepository i ProductService</p>
<ol>
<li>Utworzyć ProductCategoryRepository</li>
<li>Wstrzyknąć ProductCategoryRepository do ProductCategoryService</li>
<li>Zastąpić implementację listy w ProductCategoryService implementacją bazodanową</li>
<li>Ustawić poprawne generowanie kluczy podstawowych dla encji ProductCategory na wzór Product</li>
</ol>
<hr />
<p>https://www.baeldung.com/jpa-entities</p>
<p>https://regexr.com/</p>
<p>https://www.baeldung.com/java-bean-validation-not-null-empty-blank</p>
<p>https://www.baeldung.com/jpa-size-length-column-differences</p>
<p>https://www.baeldung.com/spring-boot-bean-validation</p>
<hr />
<p>Zadanie 6: Zmienić linki z @RequestParam na @PathVariable (wzór ProductCategoryController, pamiętać o linkach w widokach)</p>
<hr />
<p>Zadanie 7: Na podstawie Product & ProductController & save-product.html</p>
<ul>
<li>dodać walidacje do ProductCategory + messages</li>
<li>uruchomić walidację dla enpointu dodawania z danymi formularza</li>
<li>przechwycić błędy z bindingResults i przekierować je za pomocą RedirectAttributes</li>
<li>wyświetlić błędy w HTML</li>
<li>zabezpieczyć analogicznie endpoint edytujący</li>
<li>pamiętać że walidacja i binding results (parametry metody) muszą być obok siebie</li>
</ul>
<hr />
<ul>
<li>https://www.baeldung.com/intro-to-project-lombok</li>
<li>https://www.jetbrains.com/help/idea/annotation-processors-support.html</li>
<li>https://projectlombok.org/features/</li>
<li>https://projectlombok.org/features/Builder</li>
<li>https://refactoring.guru/pl/design-patterns/builder</li>
<li>https://www.baeldung.com/jpa-one-to-one</li>
<li>https://www.baeldung.com/hibernate-one-to-many</li>
<li>https://www.baeldung.com/hibernate-many-to-many</li>
<li>https://www.baeldung.com/jpa-cascade-types</li>
</ul>
<hr />
<p>Zadanie 8:</p>
<ul>
<li>utworzyć save-user.html</li>
<li>w środku znajdzie się form z polami typu input (text/number) + wyślij</li>
<li>FirstName, LastName, Email (text), PersonalNumber, TaxNumber (number)</li>
<li>2 inputy do Phones (prefix, number)</li>
<li>Addresses będzie zrealizowany jako select</li>
<li>Pod selectem będzie dodatkowy formularz z polami: city, street (text) + przycisk wyślij</li>
<li>Dodać endpoint serwujący formularz dodawania usera</li>
<li>Na stronie podglądau userów dodać przycisk prowadzący do forlumarza dodawania usera</li>
</ul>
<hr />
<p>Zadanie 9:</p>
<ul>
<li>W zakladce All users obok [show more] dodać [remove]</li>
<li>Remove będzie linkiem do adresu URL /user/remove/{id}</li>
<li>Należy utworzyć endpoint [GET] nasłuchujący na /user/remove/{id}</li>
<li>Endpoint ma uruchomić funkcję serwisową userService.removeUserById</li>
<li>Dodać funkcję w serwisie o nazwie removeUserById z parametrem Int userId</li>
<li>Funckja serwisowa powinna wykonać deleteById na repozytorium</li>
</ul>
<hr />
<ul>
<li>https://vladmihalcea.com/orphanremoval-jpa-hibernate/</li>
<li>https://www.baeldung.com/jpa-cascade-remove-vs-orphanremoval</li>
</ul>
<hr />
<p>Zadanie 9:</p>
<ul>
<li>Dodać select do formularza dodawania produktu</li>
<li>Wypełnić select danymi (pobrać listę kategorii i przekazać do fromularza)</li>
<li>Należy pamiętać aby każdy option selecta miał value ustawione na ID kategorii</li>
<li>Należy pamiętać o name dla selecta, ustawiony na "category"</li>
<li>Select wyświetla tylko nazy kategorii</li>
</ul>
<hr />
<p>Zadanie 10:</p>
<ul>
<li>Na podstawie ProductRestController utworzyć ProductRestCategoryController</li>
<li>Na podstawie ProductDto utworzyć ProductCategoryDto</li>
<li>ProductCategoryDto bedzie potrzebny do ProductRestCategoryController (przyjmowanie i zwracanie danych z endpointow)</li>
<li>Utworzyć mapper CategoryToCategoryDtoMapper (2 metody: CategoryProductDto -> CategoryProduct, CategoryProduct -> CategoryProductDto)</li>
<li>Przetestować endpointy w postmanie (2x GET, POST, PUT, DELETE)</li>
</ul>
<hr />
<ul>
<li>https://www.baeldung.com/spring-data-jpa-query</li>
<li>https://www.baeldung.com/exception-handling-for-rest-with-spring</li>
<li>https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/</li>
<li>https://www.baeldung.com/spring-security-login</li>
<li>https://spring.io/guides/gs/securing-web/</li>
<li>https://www.javadevjournal.com/spring/spring-security-userdetailsservice/</li>
<li>https://www.baeldung.com/spring-security-method-security</li>
<li>https://www.baeldung.com/spring-security-thymeleaf</li>
<li>https://www.thymeleaf.org/doc/articles/springsecurity.html</li>
<li>https://www.baeldung.com/csrf-thymeleaf-with-spring-security</li>
<li>https://www.baeldung.com/java-dto-pattern</li>
</ul>
<hr />
<p>Zadanie 11 - domowe na 24.06.23</p>
<ul>
<li>Wzorując się na rest controllerach Product i CategoryProduct utworzyć UserRestController</li>
<li>Utworzyć UserDto z polami (userId (Int), firstName (Str), lastName (Str), email (Str), role (Str), password (Str), detailsId (Int), phoneNumbersId (Int[]), addressesId (Int[]))</li>
<li>Utworzyć mapper UserToUserDtoMapper (2 metody: UserDto -> User, User -> UserDto)</li>
<li>Utworzyć endpointy (pełen CRUD dla usera -> 2x GET, POST, PUT, DELETE)</li>
<li>Przetestować endpointy w postmanie (2x GET, POST, PUT, DELETE)</li>
</ul>
<hr />
<ul>
<li>https://spring.io/guides/gs/securing-web/</li>
<li>https://www.baeldung.com/spring-security-exceptions</li>
<li>https://www.baeldung.com/spring-security-custom-authentication-failure-handler</li>
<li>https://www.baeldung.com/manually-set-user-authentication-spring-security</li>
<li>https://www.baeldung.com/spring-security-remember-me</li>
</ul>
